DOTTCostumeLib

This is a library for accessing Day of the Tentacle: Remastered costume.xml files. 

It was created to provide a more comprehensive way of accessing the sprites of the game.

A large amount of work on this was based on bgbennyboys' Double Fine Explorer: https://github.com/bgbennyboy/DoubleFine-Explorer

Double Fine Explorer is an excellent tool and provided a lot of great reference material when it came to reading MXT5 image data and converting it to RGBA colour space. Double Fine Explorer also allowed the first spritesheet of every costume to be ripped, which was instrumental in figuring out several other elements of the costume.xml format.

DOTTCostumeLib differs from Double Fine Explorer in that instead of just seeking to and ripping the first sprite sheet, it reads data in the costume.xml file to actually check how many sheets are contained. It reads the data for the individual sprites in the file too, allowing the ability to access and export the individual sprites on the sheet instead of just the sheet on its own.

CostumeOMatic

An example tool of sorts that was mainly built for testing the library. Takes a folder containing one or more costume.xml files and will parse them all. Info about the files will be logged to "./log.log", spritesheets will be logged to a folder titled after the filename of the costume.xml file while each sprite will also be saved seperately to a "Sprites" subfolder within that.


Note that the code for both of these projects is incredibly messy at the moment and due for cleanup.