﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Drawing;
using System.IO.Compression;
using DirectXTex;
using Pfim;
using System.Linq;
using System.Drawing.Imaging;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Formats.Png;
using System.Threading.Tasks.Dataflow;

namespace DOTTCostumeLib
{
	public class Sheet
	{
		public uint Width;
		public uint Height;
		public uint MXTSize; //Size of the MXT (MXT5 header + gzipped DDS data)
		public uint Unk1; //Might be decompressed size
 		public Bitmap Image;
	}
	public class Sprite
	{
		public uint SheetID;
		public uint TopLeftX;
		public uint TopLeftY;
		public uint BottomRightX;
		public uint BottomRightY;
	}
	public class DOTTSkin
	{
		public string Name;

		public uint NumSheets;
		public uint NumSprites;

		public List<Sheet> Sheets;
		public List<Sprite> Sprites;

		//Get a single sprite by index
		public Bitmap GetSpriteImage(int sprite)
		{
			Sprite spriteInfo = Sprites[sprite];

			Sheet sheet = Sheets[(int)spriteInfo.SheetID];

			Rectangle CropArea = new Rectangle((int)spriteInfo.TopLeftX, (int)spriteInfo.TopLeftY, (int)spriteInfo.BottomRightX, (int)spriteInfo.BottomRightY);
			Bitmap spriteImage = sheet.Image.Clone(CropArea, sheet.Image.PixelFormat);
			return spriteImage;
		}

		//Get a list containing all individual sprites
		public List<Bitmap> GetAllSpriteImages()
		{
			List<Bitmap> sprites = new List<Bitmap>();
			for (int i = 0; i<NumSprites; i++)
			{
				sprites.Add(GetSpriteImage(i));
			}
			return sprites;
		}
	}
	public class DOTTCostume
	{
		public uint CostumeNumber;
		public int Unk3; //Don't know what it is, but we need it to calculate the size of one of the blocks correctly. Body parts?
		public uint AnimCount;

		public DOTTSkin Skin;

		public static DOTTCostume Read(string file)
		{
			return Read(File.OpenRead(file));
		}

		public static DOTTCostume Read(Stream stream)
		{
			DOTTCostume costume = new DOTTCostume();
			using ( BinaryReader br = new BinaryReader(stream) )
			{
				//Header
				costume.CostumeNumber = br.ReadUInt32();
				_ = br.ReadInt32();
				_ = br.ReadUInt64();
				costume.Unk3 = br.ReadInt32();
				_ = br.ReadInt32();
				costume.AnimCount = br.ReadUInt32();
				_ = br.ReadInt32();
				_ = br.ReadInt32();


				br.BaseStream.Align(16);

				costume.Skin = new DOTTSkin();
				costume.Skin.Name = br.ReadNTString();
				br.BaseStream.Seek(1, SeekOrigin.Current); //We don't include the null terminator in ReadNTString but alignment in this engine assumed it's included
				br.BaseStream.Align(16);

				long StartOfSkinInfo = br.BaseStream.Position;
				_ = br.ReadInt32();
				costume.Skin.NumSheets = br.ReadUInt32();
				_ = br.ReadInt32();
				costume.Skin.NumSprites = br.ReadUInt32();
				br.BaseStream.Seek(StartOfSkinInfo, SeekOrigin.Begin);
				br.BaseStream.Seek((0x14 * costume.Unk3), SeekOrigin.Current);

				br.BaseStream.Align(16);

				//Skip some data regarding animations
				br.BaseStream.Seek((0x14 * costume.AnimCount), SeekOrigin.Current);

				br.BaseStream.Align(16);

				br.BaseStream.Seek(0x20, SeekOrigin.Current); //Not certain what these 20 bytes are for.

				//NOW FOR WHAT WE'RE ALL HERE FOR
				costume.Skin.Sheets = new List<Sheet>();
				for (int i=0; i<costume.Skin.NumSheets; i++)
				{
					Sheet sheet = new Sheet();
					sheet.Width = br.ReadUInt32();
					sheet.Height = br.ReadUInt32();
					sheet.MXTSize = br.ReadUInt32();
					sheet.Unk1 = br.ReadUInt32();
					costume.Skin.Sheets.Add(sheet);
				}

				costume.Skin.Sprites = new List<Sprite>();
				for (int i = 0; i < costume.Skin.NumSprites; i++)
				{
					Sprite sprite = new Sprite();
					sprite.SheetID = br.ReadUInt32();
					sprite.TopLeftX = br.ReadUInt32();
					sprite.TopLeftY = br.ReadUInt32();
					sprite.BottomRightX = br.ReadUInt32();
					sprite.BottomRightY = br.ReadUInt32();
					costume.Skin.Sprites.Add(sprite);
				}

				//Okay, we have all the info we need. Don't care about any other data in the file for now so finally we can just
				//skip to the images we need.
				byte[] ImageHeader = new byte[4] { 0x4D, 0x58, 0x54, 0x35 };

				
				br.BaseStream.Seek(br.BaseStream.FindPosition(ImageHeader), SeekOrigin.Begin);

				//Now at the first image.

				//Read all the sheets!
				for ( int i = 0; i<costume.Skin.Sheets.Count; i++)
				{
					Sheet curSheet = costume.Skin.Sheets[i];

					byte[] MXTData;
					MXTData = br.ReadBytes((int)curSheet.MXTSize);

					curSheet.Image = MXT2Bitmap(MXTData, curSheet.MXTSize);

					br.BaseStream.Align(16);
				}
				
			}

			return costume;
		}

		//Convert an MXT to a bitmap, convert colour space from YCoCg to RGBA
		private static Bitmap MXT2Bitmap(byte[] MXTData, uint MXTSize)
		{
			using (MemoryStream ms = new MemoryStream(MXTData))
			{
				using ( BinaryReader br = new BinaryReader(ms) )
				{
					br.BaseStream.Seek(4, SeekOrigin.Begin); //Skip MXT5 magic number
					uint ImageWidth = br.ReadUInt32();
					uint ImageHeight = br.ReadUInt32();

					uint ImageDecompressedSize = ImageWidth * ImageHeight;
					
					br.BaseStream.Seek(4, SeekOrigin.Current); //Unknown data

					uint GZipLength = MXTSize - 16; //Minus the 16 byte MXT5 header gives us just the size of the GZip data



					//We're now at compressed DDS data - decompress

					byte[] ImageData; //Actual image data
					using (var stream_compressed = new MemoryStream(br.ReadBytes((int)GZipLength)))
					using (var stream_zip = new GZipStream(stream_compressed, CompressionMode.Decompress))
					using (var stream_result = new MemoryStream())
					{
						stream_zip.CopyTo(stream_result);
						ImageData = stream_result.ToArray();
					}

					//The image data is headerless - we need to construct a DDS header.
					DirectXTexUtility.DDSHeader header = new DirectXTexUtility.DDSHeader();
					header.Width = ImageWidth;
					header.Height = ImageHeight;
					header.Flags = (DirectXTexUtility.DDSHeader.HeaderFlags)(0x1 | 0x2 | 0x4 | 0x1000 | 0x80000);
					header.PixelFormat = new DirectXTexUtility.DDSHeader.DDSPixelFormat(0, 0, 0, 0, 0, 0, 0, 0);
					header.Size = 124;
					header.PitchOrLinearSize = ImageDecompressedSize;

					header.PixelFormat.Flags = 0x4;
					header.PixelFormat.Size = 32;
					header.PixelFormat.FourCC = 0x35545844;
					header.Caps = 0x1000;

					//Get header data bytes
					byte[] DDSHeader = DirectXTexUtility.EncodeDDSHeader(header, new DirectXTexUtility.DX10Header());

					//Append our image data to the constructed header.
					byte[] FullDDSData = DDSHeader.Concat(ImageData).ToArray();

					PfimConfig config = new PfimConfig(applyColorMap: false);
					Dds DDS;
					using (MemoryStream mem = new MemoryStream(FullDDSData))
					{
						//Using a MemoryStream here is a workaround, creating a DDS from a byte array seems broken in Pfim.
						DDS = Dds.Create(mem, config);
					}


					//God I hate YCoCg..
					for ( int i = 0; i<DDS.DataLen; i+=DDS.BytesPerPixel)
					{
						byte A1 = DDS.Data[i];
						byte CG = (byte)(DDS.Data[i + 1]-1);
						byte CO = DDS.Data[i + 2];
						byte Y = DDS.Data[i + 3];

						ConvertYCoCgToRGB(Y, CO, CG, A1, out byte R, out byte G, out byte B, out byte A);
						
						DDS.Data[i] = R;
						DDS.Data[i+1] = G;
						DDS.Data[i+2] = B;
						DDS.Data[i+3] = A;
					}

					//Strip out line padding, if any, for ImageSharp
					int TightStride = DDS.Width * DDS.BitsPerPixel / 8;
					byte[] newData;
					if (DDS.Stride != TightStride)
					{
						newData = new byte[DDS.Height * TightStride];
						for (int i = 0; i < DDS.Height; i++)
						{
							Buffer.BlockCopy(DDS.Data, i * DDS.Stride, newData, i * TightStride, TightStride);
						}
					}
					else
					{
						newData = DDS.Data;
					}

					Bitmap bmp;
					using (MemoryStream imgstream = new MemoryStream())
					{
						PngEncoder encoder = new PngEncoder();
						SixLabors.ImageSharp.Image img = SixLabors.ImageSharp.Image.LoadPixelData<Rgba32>(newData, DDS.Width, DDS.Height);
						img.Save(imgstream, encoder);
						ms.Seek(0, SeekOrigin.Begin);

						bmp = new Bitmap(imgstream);
					}

					return bmp;
				}
			}
		}

		//Adapted from https://github.com/bgbennyboy/DoubleFine-Explorer/blob/46ac3ec8df5279484d5802784e021c9a4f6fba3e/uDFExplorer_Funcs.pas#L1040
		private static void ConvertYCoCgToRGB(byte Y, byte CO, byte CG, byte A1, out byte R, out byte G, out byte B, out byte A)
		{	
			int CoInt = CO - 128;

			int CgInt = CG - 128;

			R = (byte)Math.Clamp(Y + CoInt - CgInt, 0, 255);
			
			G = (byte)Math.Clamp(Y + CgInt, 0, 255);
			
			B = (byte)Math.Clamp(Y - CoInt - CgInt, 0, 255);

			A = A1;
		}

	}
}
