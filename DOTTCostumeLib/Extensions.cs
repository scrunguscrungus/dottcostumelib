﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace DOTTCostumeLib
{
	public static class Extensions
	{
		public static void Align(this Stream bw, int alignment)
		{
			bw.Seek((int)(-bw.Position % alignment + alignment) % alignment, SeekOrigin.Current);
		}

		public static string ReadNTString(this BinaryReader br)
		{
			//int stringLength = 1;
			int stringLength = 0; //stringLength 1 worked fine before but stopped working for some reason...

			long startpos = br.BaseStream.Position;

			while ( br.PeekChar() != '\0')
			{
				stringLength++;
				br.BaseStream.Seek(1, SeekOrigin.Current);
			}
			br.BaseStream.Seek(startpos, SeekOrigin.Begin);

			return Encoding.Default.GetString(br.ReadBytes(stringLength));
		}

        //https://stackoverflow.com/a/1472689
        public static long FindPosition(this Stream stream, byte[] byteSequence)
        {
            /*
            if (byteSequence.Length > stream.Length)
                return -1;

            byte[] buffer = new byte[byteSequence.Length];

            using (BufferedStream bufStream = new BufferedStream(stream, byteSequence.Length))
            {
                int i;
                while ((i = bufStream.Read(buffer, 0, byteSequence.Length)) == byteSequence.Length)
                {
                    if (byteSequence.SequenceEqual(buffer))
                        return bufStream.Position - byteSequence.Length;
                    else
                        bufStream.Position -= byteSequence.Length - PadLeftSequence(buffer, byteSequence);
                }
            }

            return -1;
            */

            if (byteSequence.Length > stream.Length)
                return -1;

            int hits = 0;
            using ( BinaryReader br = new BinaryReader(stream, Encoding.Default, true))
			{
                br.BaseStream.Seek(0, SeekOrigin.Begin);
                int readByte = 0;
                while ( readByte != -1 )
				{
                    readByte = br.ReadByte();
                    if (readByte == byteSequence[hits])
					{
                        //Console.WriteLine("Hit: {0:X} at {1:X} matches {2:X}", readByte, br.BaseStream.Position, byteSequence[hits]);
                        hits++;
                    }
                    else
					{
                        hits = 0;
                    }

                    if (hits == byteSequence.Length)
                        return br.BaseStream.Position-byteSequence.Length;
				}
			}

            return -1;
        }

        private static int PadLeftSequence(byte[] bytes, byte[] seqBytes)
        {
            int i = 1;
            while (i < bytes.Length)
            {
                int n = bytes.Length - i;
                byte[] aux1 = new byte[n];
                byte[] aux2 = new byte[n];
                Array.Copy(bytes, i, aux1, 0, n);
                Array.Copy(seqBytes, aux2, n);
                if (aux1.SequenceEqual(aux2))
                    return i;
                i++;
            }
            return i;
        }

    }
}
