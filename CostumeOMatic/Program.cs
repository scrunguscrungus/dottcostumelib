﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Text;

using DOTTCostumeLib;
namespace CostumeOMatic
{
	class Program
	{
		static void Main(string[] args)
		{
			string InputDir = Console.ReadLine().Trim('"');
			string OutFile = "./out.log";

			using ( StreamWriter sw = new StreamWriter(File.Open(OutFile, FileMode.Create)) )
			{
				foreach (string file in Directory.EnumerateFiles(InputDir, "*.*", SearchOption.AllDirectories))
				{
					DOTTCostume costume = DOTTCostume.Read(file);
					string filename = Path.GetFileName(file);
					Console.WriteLine("Dumping {0}...", filename);
					DumpInfo(filename, sw, costume);

					int i = 0;
					
					foreach( Sheet sheet in costume.Skin.Sheets )
					{
						if (!Directory.Exists(filename))
							Directory.CreateDirectory(filename);

						sheet.Image.Save($"{filename}/sheet-{i}.png");
						i++;
					}

					i = 0;
					foreach ( Bitmap img in costume.Skin.GetAllSpriteImages() )
					{
						string dir = filename + "/sprites";
						if (!Directory.Exists(dir))
							Directory.CreateDirectory(dir);

						img.Save($"{dir}/sprite-{i}.png");
						i++;
					}
				}
			}
		}

		static void DumpInfo(string filename, TextWriter stream, DOTTCostume costume)
		{
			stream.WriteLine($"{filename}:");
			foreach (FieldInfo field in typeof(DOTTCostume).GetFields())
			{
				stream.WriteLine(GetFieldInfo(stream, costume, field, 1));
			}
			stream.WriteLine(CheckOutliers(costume));
			stream.WriteLine();	
		}

		private static string GetFieldInfo(TextWriter stream, object? obj, FieldInfo field, int indentation)
		{
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < indentation; i++)
				sb.Append("\t");
			if (field.FieldType == typeof(DOTTSkin))
			{
				DOTTSkin skin = (DOTTSkin)field.GetValue(obj);
				
				sb.AppendFormat("Skin {0}:\n", skin.Name);
				foreach (FieldInfo subfield in typeof(DOTTSkin).GetFields())
				{
					sb.AppendLine(GetFieldInfo(stream, skin, subfield, indentation+1));
				}
			}
			else if (field.FieldType == typeof(Sheet))
			{
				Sheet skin = (Sheet)field.GetValue(obj);

				sb.AppendFormat("Sheet:\n");
				foreach (FieldInfo subfield in typeof(Sheet).GetFields())
				{
					sb.AppendLine(GetFieldInfo(stream, skin, subfield, indentation + 1));
				}
			}
			else if (field.FieldType == typeof(Sprite))
			{
				Sprite skin = (Sprite)field.GetValue(obj);

				sb.AppendFormat("Sprite:\n");
				foreach (FieldInfo subfield in typeof(Sprite).GetFields())
				{
					sb.AppendLine(GetFieldInfo(stream, skin, subfield, indentation + 1));
				}
			}
			else if (field.FieldType.IsGenericType && field.FieldType.GetGenericTypeDefinition().Name == "List`1")
			{
				sb.AppendLine($"{field.Name}:");
				int count = (int)field.FieldType.GetProperty("Count").GetValue(field.GetValue(obj));
				for (int i = 0; i < indentation; i++)
					sb.Append("\t");
				sb.AppendLine("{");
				for ( int i = 0; i<count; i++)
				{
					object[] index = { i };
					object item = field.FieldType.GetProperty("Item").GetValue(field.GetValue(obj), index);
					for (int j = 0; j < indentation+1; j++)
						sb.Append("\t");
					sb.AppendLine("{");
					foreach (FieldInfo subfield in item.GetType().GetFields())
					{
						for (int j = 0; j < indentation+2; j++)
							sb.Append("\t");

						sb.AppendLine(GetFieldInfo(stream, item, subfield, indentation + 1));
						
					}
					for (int j = 0; j < indentation+1; j++)
						sb.Append("\t");
					sb.AppendLine("}");
				}
				for (int i = 0; i < indentation; i++)
					sb.Append("\t");
				sb.AppendLine("}");
			}
			else
			{
				sb.AppendFormat("{0}: {1} ({1:X})", field.Name, field.GetValue(obj));
			}

			return sb.ToString();
		}

		static string CheckOutliers(DOTTCostume costume)
		{
			StringBuilder sb = new StringBuilder();
			/*
			if (costume.Unk1 != 44)
				sb.AppendLine(string.Format("\tUnk1: expected 44, got {0} ({0:X})", costume.Unk1));

			if ( costume.Unk2 != 0x4110000040F00000)
				sb.AppendLine(string.Format("\tUnk2: expected 0x4110000040F00000, got {0} ({0:X})", costume.Unk2));

			if (costume.Unk4 != 0x2C && costume.Unk4 != 0x3C)
				sb.AppendLine(string.Format("\tUnk4: expected 0x2C or 0x3C, got {0} ({0:X})", costume.Unk4));
			*/
			//if (costume.Unk5 != 0x44 && costume.Unk5 != 0x74)
			//	sb.AppendLine(string.Format("\tUnk5: expected 0x44 or 0x74, got {0} ({0:X})", costume.Unk5));

			if (sb.ToString() != string.Empty)
				sb.Insert(0, "OUTLIERS DETECTED:\n");

			return sb.ToString();
		}
	}
}
